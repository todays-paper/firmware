#ifndef WAVESHARE_H
#define WAVESHARE_H

#include <types.h>

class Waveshare {
public:
	Waveshare();
	void init();
	void draw2(uint8_t highPixel, uint8_t lowPixel);
	void render();
	void show();

private:
	void epdInitSPI();
	void epdReset();
	void epdSend1(uint8_t c, uint8_t v1);
	void epdSend2(uint8_t c, uint8_t v1, uint8_t v2);
	void epdSend3(uint8_t c, uint8_t v1, uint8_t v2, uint8_t v3);
	void epdSend4(uint8_t c, uint8_t v1, uint8_t v2, uint8_t v3, uint8_t v4);
	void epdSendCommand(uint8_t command);
	void epdSendData(uint8_t data);
	void epdSpiTransferCallback(uint8_t data);
	void epdWaitUntilIdle();
};


#endif // WAVESHARE_H
