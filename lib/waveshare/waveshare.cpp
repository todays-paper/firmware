#include <Arduino.h>
#include <types.h>
#include "waveshare.h"

/* SPI pin definition --------------------------------------------------------*/
#define PIN_SPI_SCK  13
#define PIN_SPI_DIN  14
#define PIN_SPI_CS   15
#define PIN_SPI_BUSY 25//19
#define PIN_SPI_RST  26//21
#define PIN_SPI_DC   27//22

#define GPIO_PIN_SET   1
#define GPIO_PIN_RESET 0

Waveshare::Waveshare() {
}

void Waveshare::draw2(uint8_t highPixel, uint8_t lowPixel) {
	epdSendData((highPixel << 4) | (lowPixel & 0xF));
}

void Waveshare::init() {
	epdInitSPI();
	epdReset();
	epdSend2(0x01, 0x37, 0x00);            //POWER_SETTING
	epdSend2(0x00, 0xCF, 0x08);            //PANEL_SETTING
	epdSend3(0x06, 0xC7, 0xCC, 0x28);      //BOOSTER_SOFT_START
	epdSendCommand(0x4);                   //POWER_ON
	epdWaitUntilIdle();
	epdSend1(0x30, 0x3C);                  //PLL_CONTROL
	epdSend1(0x41, 0x00);                  //TEMPERATURE_CALIBRATION
	epdSend1(0x50, 0x77);                  //VCOM_AND_DATA_INTERVAL_SETTING
	epdSend1(0x60, 0x22);                  //TCON_SETTING
	epdSend4(0x61, 0x02, 0x80, 0x01, 0x80);//TCON_RESOLUTION
	epdSend1(0x82, 0x1E);                  //VCM_DC_SETTING: decide by LUT file
	epdSend1(0xE5, 0x03);                  //FLASH MODE

	epdSendCommand(0x10);                 //DATA_START_TRANSMISSION_1
	delay(2);
}

/* Show image and turn to deep sleep mode (7.5 and 7.5b e-Paper) -------------*/
void Waveshare::show() {
	// Refresh
	epdSendCommand(0x12); // DISPLAY_REFRESH
	delay(100);
	epdWaitUntilIdle();

	// Sleep
	epdSendCommand(0x02); // POWER_OFF
	epdWaitUntilIdle();
	epdSend1(0x07, 0xA5); // DEEP_SLEEP
}

void Waveshare::epdInitSPI() {
	pinMode(PIN_SPI_BUSY,  INPUT);
	pinMode(PIN_SPI_RST , OUTPUT);
	pinMode(PIN_SPI_DC  , OUTPUT);

	pinMode(PIN_SPI_SCK, OUTPUT);
	pinMode(PIN_SPI_DIN, OUTPUT);
	pinMode(PIN_SPI_CS , OUTPUT);

	digitalWrite(PIN_SPI_CS , HIGH);
	digitalWrite(PIN_SPI_SCK, LOW);
}

void Waveshare::epdReset() {
	digitalWrite(PIN_SPI_RST, LOW);
	delay(200);

	digitalWrite(PIN_SPI_RST, HIGH);
	delay(200);
}

/* Sending a byte as a command -----------------------------------------------*/
void Waveshare::epdSendCommand(uint8_t command) {
    digitalWrite(PIN_SPI_DC, LOW);
    epdSpiTransferCallback(command);
}

/* Sending a byte as a data --------------------------------------------------*/
void Waveshare::epdSendData(uint8_t data) {
    digitalWrite(PIN_SPI_DC, HIGH);
    epdSpiTransferCallback(data);
}

/* Send a one-argument command -----------------------------------------------*/
void Waveshare::epdSend1(uint8_t c, uint8_t v1) {
	epdSendCommand(c);
	epdSendData(v1);
}

/* Send a two-arguments command ----------------------------------------------*/
void Waveshare::epdSend2(uint8_t c, uint8_t v1, uint8_t v2) {
    epdSendCommand(c);
    epdSendData(v1);
    epdSendData(v2);
}

/* Send a three-arguments command --------------------------------------------*/
void Waveshare::epdSend3(uint8_t c, uint8_t v1, uint8_t v2, uint8_t v3) {
    epdSendCommand(c);
    epdSendData(v1);
    epdSendData(v2);
    epdSendData(v3);
}

/* Send a four-arguments command ---------------------------------------------*/
void Waveshare::epdSend4(uint8_t c, uint8_t v1, uint8_t v2, uint8_t v3, uint8_t v4) {
    epdSendCommand(c);
    epdSendData(v1);
    epdSendData(v2);
    epdSendData(v3);
    epdSendData(v4);
}

/* The procedure of sending a byte to e-Paper by SPI -------------------------*/
void Waveshare::epdSpiTransferCallback(uint8_t data) {
	//SPI.beginTransaction(spi_settings);
	digitalWrite(PIN_SPI_CS, GPIO_PIN_RESET);

	for (int i = 0; i < 8; i++) {
		if ((data & 0x80) == 0) {
			digitalWrite(PIN_SPI_DIN, GPIO_PIN_RESET);
		} else {
			digitalWrite(PIN_SPI_DIN, GPIO_PIN_SET);
		}

		data <<= 1;
		digitalWrite(PIN_SPI_SCK, GPIO_PIN_SET);
		digitalWrite(PIN_SPI_SCK, GPIO_PIN_RESET);
	}

	//SPI.transfer(data);
	digitalWrite(PIN_SPI_CS, GPIO_PIN_SET);
	//SPI.endTransaction();
}

/* Waiting the e-Paper is ready for further instructions ---------------------*/
void Waveshare::epdWaitUntilIdle() {
	//0: busy, 1: idle
	while(digitalRead(PIN_SPI_BUSY) == 0) {
		delay(100);
	}
}
