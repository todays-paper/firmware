#!/bin/sh

set -e

MODEL=$1
FW_URL="https://fw.todays-paper.ink/${MODEL}/firmware.bin"
FW_VERSION=$(git describe --always --dirty)
FW_SIZE=$(wc -c < ".pio/build/${MODEL}/firmware.bin")

DATA_URL="https://fw.todays-paper.ink/${MODEL}/spiffs.bin"
DATA_VERSION=$(sha256sum .pio/build/epdiy_v5/spiffs.bin | cut -d ' ' -f 1)
DATA_SIZE=$(wc -c < ".pio/build/${MODEL}/spiffs.bin")

jq --null-input \
  --arg fw_version "${FW_VERSION}" \
  --arg fw_url "${FW_URL}" \
  --argjson fw_size ${FW_SIZE} \
  --arg data_version "${DATA_VERSION}" \
  --arg data_url "${DATA_URL}" \
  --argjson data_size ${DATA_SIZE} \
  '{"app": {"version": $fw_version, "url": $fw_url, "size": $fw_size}, "data": {"version": $data_version, "url": $data_url, "size": $data_size}}' \
  > .pio/build/${MODEL}/firmware.json
