#include "webserver.h"

#include <cJSON.h>
#include <esp_http_server.h>
#include <esp_log.h>

#include "settings.h"
#include "wifi.h"

// Configure the board:
// curl -d '{"fetch_url":"https://fetch.todays-paper.ink/paper"}' -H
// "Content-Type: application/json" -X POST http://192.168.0.0/config
namespace {
	const char *TAG = "WebServer";
	httpd_handle_t server = nullptr;
	char buf[255];

	void setConfig(const char *config, int len) {
		cJSON *json = cJSON_ParseWithLength(config, len);
		if (!json) {
			return;
		}
		cJSON *fetchUrl = cJSON_GetObjectItem(json, "fetch_url");
		if (fetchUrl) {
			auto *url = cJSON_GetStringValue(fetchUrl);
			settings::write("nvs", "fetch_url", url);
		}
		cJSON *display = cJSON_GetObjectItem(json, "display");
		if (display) {
			settings::write("board", "display", cJSON_GetStringValue(display));
		}
		cJSON *vcomVoltage = cJSON_GetObjectItem(json, "vcom_voltage");
		if (vcomVoltage) {
			settings::write("board", "vcom_voltage", (uint16_t)vcomVoltage->valueint);
		}
	}

	esp_err_t resetHandler(httpd_req_t *req) {
		const char resp[] = "Wifi reset success";
		httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
		wifi::reset();
		esp_restart();
		return ESP_OK;
	}

	esp_err_t setConfigHandler(httpd_req_t *req) {
		int ret;
		if (req->content_len >= sizeof(buf)) {
			return httpd_resp_send_500(req);
		}

		// Read the data for the request
		if ((ret = httpd_req_recv(req, buf, req->content_len)) <= 0) {
			return ESP_FAIL;
		}

		setConfig(buf, ret);

		const char resp[] = "Success";
		httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
		return ESP_OK;
	}

	// URI handler structure for GET /reset
	const httpd_uri_t uri_reset = {
	    .uri = "/reset",
	    .method = HTTP_GET,
	    .handler = resetHandler,
	    .user_ctx = NULL,
	};

	const httpd_uri_t uri_config_post = {
	    .uri = "/config",
	    .method = HTTP_POST,
	    .handler = setConfigHandler,
	    .user_ctx = NULL,
	};
}  // namespace

void webserver::start() {
	// Generate default configuration
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();

	// Start the httpd server
	ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
	if (httpd_start(&server, &config) != ESP_OK) {
		ESP_LOGI(TAG, "Error starting server!");
		return;
	}

	// Register URI handlers
	httpd_register_uri_handler(server, &::uri_reset);
	httpd_register_uri_handler(server, &::uri_config_post);
}
