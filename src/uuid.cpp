#include "uuid.h"

#include <stdio.h>
#include <sys/random.h>

#include <cstring>

using namespace uuid;

uuid_t uuid_t::new_v4() {
	uuid_t uuid;
	getrandom(uuid.val, sizeof(uuid.val), 0);
	uuid.val[6] = 0x40 | (uuid.val[6] & 0xF);  // Set version 4

	return uuid;
}

bool uuid_t::parse_str(const char *str) {
	int item_matched =
	    sscanf(str,
	           "%2hhx%2hhx%2hhx%2hhx-%2hhx%hhx-%2hhx%2hhx-%2hhx%2hhx-%2hhx%2hhx%"
	           "2hhx%2hhx%2hhx%2hhx",
	           &val[0], &val[1], &val[2], &val[3], &val[4], &val[5], &val[6],
	           &val[7], &val[8], &val[9], &val[10], &val[11], &val[12], &val[13],
	           &val[14], &val[15]);
	if (item_matched < 16 || item_matched == EOF) {
		return false;
	}
	return true;
}

int uuid_t::to_str(char *buffer, size_t len) {
	assert(len > 36);
	return snprintf(
	    buffer, len,
	    "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
	    val[0], val[1], val[2], val[3], val[4], val[5], val[6], val[7], val[8],
	    val[9], val[10], val[11], val[12], val[13], val[14], val[15]);
}

int uuid_t::to_str(uuid_str_t buffer) {
	return to_str(buffer, sizeof(uuid_str_t));
}
