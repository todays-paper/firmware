extern "C" {
#include <epd_board_specific.h>
}
#include <img.h>

#include "board.h"
#include "esp_log.h"
#include "firasans_12.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define FONT FiraSans_12
namespace {
	static EpdiyHighlevelState *hl = nullptr;
}

void imgInit(EpdiyHighlevelState *hl) {
	::hl = hl;
}

void imgRenderBuffer(const char *buffer) {
	const ImgHeader *header = (ImgHeader *)buffer;
	const uint8_t *paper_data = (uint8_t *)(buffer + sizeof(ImgHeader));
	EpdRect paper_area = {
	    .x = (epd_rotated_display_width() - (header->width)) / 2,
	    .y = (epd_rotated_display_height() - (header->height)) / 2,
	    .width = header->width,
	    .height = header->height,
	};
	ESP_LOGI("EPDIY", "Epaper size %d width %d height %d, bbp %d, dataSize %d",
	         header->size, header->width, header->height, header->bpp,
	         header->dataSize);

	epd_copy_to_framebuffer(paper_area, paper_data, epd_hl_get_framebuffer(hl));
}

void renderProvisioning(esp_qrcode_handle_t qrcode) {
	EpdRect rect = {
	    .x = 0,
	    .y = 0,
	    .width = 10,
	    .height = 10,
	};
	int size = esp_qrcode_get_size(qrcode);
	int cursor_x = (epd_rotated_display_width() - (size * rect.width)) / 2;
	int cursor_y = (epd_rotated_display_height() - (size * rect.height)) / 2;
	int border = 2;
	uint8_t *fb = epd_hl_get_framebuffer(hl);

	for (int y = -border; y < size + border; y += 1) {
		for (int x = -border; x < size + border; x += 1) {
			if (esp_qrcode_get_module(qrcode, x, y)) {
				rect.x = cursor_x + (x * rect.width);
				rect.y = cursor_y + (y * rect.height);
				epd_fill_rect(rect, 0, fb);
			}
		}
	}

	EpdFontProperties font_props = epd_font_properties_default();
	font_props.flags = EPD_DRAW_ALIGN_CENTER;
	cursor_x = epd_rotated_display_width() / 2;
	cursor_y = 100;
	epd_write_string(&FONT,
	                 "Download \"ESP BLE Provisioning\" from App Store or Google "
	                 "Play\nand scan this QR-code:",
	                 &cursor_x, &cursor_y, fb, &font_props);
}
