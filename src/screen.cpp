#include <esp_log.h>
#include <img.h>
#include <screen.h>

#include <cassert>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

namespace {
	const char *TAG = "Screen";
}
extern const EpdBoardDefinition epd_board_todays_paper;

EpdiyScreen::EpdiyScreen() {
	epd_set_board(&epd_board_todays_paper);
}

void EpdiyScreen::init(EpdInitOptions options, std::string_view model) {
	assert(m_state == State::DEACTIVATED);
	if (model.compare("ED060XC3") == 0) {
		epd_set_display(1024, 758);
	} else if (model.compare("ED060KC1") == 0) {
		epd_set_display(1448, 1072);
	} else {
		esp_system_abort("Unsupported display model");
	}

	epd_init(options);
	m_hl = epd_hl_init(&epdiy_ED060XC3);
	m_state = State::POWERED_OFF;
	imgInit(&m_hl);
}

void EpdiyScreen::deinit() {
	if (m_state == State::DEACTIVATED) {
		return;
	}
	if (m_state == State::POWERED_ON) {
		powerOff();
	}
	epd_deinit();
	m_state = State::DEACTIVATED;
}

void EpdiyScreen::powerOn() {
	assert(m_state != State::DEACTIVATED);
	if (m_state == State::POWERED_ON) {
		return;
	}
	epd_poweron();
	m_state = State::POWERED_ON;
}

void EpdiyScreen::powerOff() {
	if (m_state != State::POWERED_ON) {
		return;
	}
	epd_poweroff();
	m_state = State::POWERED_OFF;
}

void EpdiyScreen::renderFromFramebuffer() {
	auto old_state = m_state;
	powerOn();
	auto temperature = epd_ambient_temperature();
	epd_clear();
	TickType_t start = xTaskGetTickCount();
	epd_hl_update_screen(&m_hl, MODE_GL16, temperature);
	TickType_t time = xTaskGetTickCount() - start;
	ESP_LOGI(TAG, "Rendering took %d ms", pdTICKS_TO_MS(time));
	if (old_state == State::POWERED_OFF) {
		// Power was off when we started, turn it off again
		powerOff();
	}
}
