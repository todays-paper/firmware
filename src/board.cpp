#include <board.h>
#include <settings.h>

#include "driver/gpio.h"
#include "esp_adc_cal.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_system.h"
#include "string.h"

extern "C" {
#include <epd_board_specific.h>
}

namespace {
	const char *TAG = "Board";
	constexpr auto uS_TO_S_FACTOR =
	    1'000'000;  // Conversion factor for micro seconds to seconds

	IScreen *screen = nullptr;
	char displayModel[16];

	uint8_t boardRevision = 0;

	struct {
		adc1_channel_t batt_v_in_adc;
		gpio_num_t batt_v_in_gpio;
		gpio_num_t ext_pwr_detect;
		gpio_num_t io_exp_intr;
		gpio_num_t i2c_scl;
		gpio_num_t i2c_sda;
		uint8_t bat_chrg;
		uint8_t bat_stdby;
	} pins;

	constexpr uint8_t PCA_PIN_P06 = 0x40;
	constexpr uint8_t PCA_PIN_P07 = 0x80;

	void printWakeupReason() {
		esp_sleep_wakeup_cause_t wakeup_reason;

		wakeup_reason = esp_sleep_get_wakeup_cause();

		switch (wakeup_reason) {
			case ESP_SLEEP_WAKEUP_EXT0:
				ESP_LOGI(TAG, "Wakeup caused by external signal using RTC_IO");
				break;
			case ESP_SLEEP_WAKEUP_EXT1:
				ESP_LOGI(TAG, "Wakeup caused by external signal using RTC_CNTL");
				break;
			case ESP_SLEEP_WAKEUP_TIMER:
				ESP_LOGI(TAG, "Wakeup caused by timer");
				break;
			case ESP_SLEEP_WAKEUP_TOUCHPAD:
				ESP_LOGI(TAG, "Wakeup caused by touchpad");
				break;
			case ESP_SLEEP_WAKEUP_ULP:
				ESP_LOGI(TAG, "Wakeup caused by ULP program");
				break;
			default:
				ESP_LOGI(TAG, "Wakeup was not caused by deep sleep: %d", wakeup_reason);
				break;
		}
	}

	void setInput(gpio_num_t gpio) {
		gpio_config_t io_conf = {
		    .pin_bit_mask = (1ULL << gpio),
		    .mode = GPIO_MODE_INPUT,
		    .pull_up_en = GPIO_PULLUP_DISABLE,
		    .pull_down_en = GPIO_PULLDOWN_DISABLE,
		    .intr_type = GPIO_INTR_DISABLE,
		};
		gpio_config(&io_conf);
	}

	float batteryAdcFactor() {
		if (::boardRevision <= 2) {
			constexpr auto DIVIDER_H = 680.f;  // 680 kOhm
			constexpr auto DIVIDER_L = 200.f;  // 200 kOhm
			constexpr float DAMPENING = (DIVIDER_L / (DIVIDER_H + DIVIDER_L));
			constexpr float FACTOR = 1 / DAMPENING;
			return FACTOR;
		}
		// Rev 3+
		constexpr auto DIVIDER_H = 10.f;  // 10 MOhm
		constexpr auto DIVIDER_L = 3.f;   // 3 MOhm
		constexpr float DAMPENING = (DIVIDER_L / (DIVIDER_H + DIVIDER_L));
		constexpr float FACTOR = 1 / DAMPENING;
		return FACTOR;
	}
}  // namespace

void board::init(IScreen *screen) {
	::screen = screen;
	auto err = settings::get("board", "board_revision", &::boardRevision);
	if (err != settings::Error::OK) {
		ESP_LOGW(TAG, "Unable to read board revision!");
		::boardRevision = 1;
	} else {
		ESP_LOGI(TAG, "Revision: %u", ::boardRevision);
	}

	if (boardRevision == 1) {
		pins = {
		    .batt_v_in_adc = ADC1_CHANNEL_6,
		    .batt_v_in_gpio = GPIO_NUM_34,
		    .ext_pwr_detect = GPIO_NUM_36,
		    .io_exp_intr = GPIO_NUM_35,
		    .i2c_scl = GPIO_NUM_33,
		    .i2c_sda = GPIO_NUM_32,
		    .bat_chrg = 0,   // N/A
		    .bat_stdby = 0,  // N/A
		};
	} else if (boardRevision >= 2 && boardRevision <= 3) {
		pins = {
		    .batt_v_in_adc = ADC1_CHANNEL_0,
		    .batt_v_in_gpio = GPIO_NUM_36,
		    .ext_pwr_detect = GPIO_NUM_34,
		    .io_exp_intr = GPIO_NUM_35,
		    .i2c_scl = GPIO_NUM_13,
		    .i2c_sda = GPIO_NUM_14,
		    .bat_chrg = PCA_PIN_P06,
		    .bat_stdby = PCA_PIN_P07,
		};
	} else {
		esp_system_abort("Unsupported board revision");
	}
	::setInput(pins.ext_pwr_detect);
	::setInput(pins.io_exp_intr);
	::setInput(pins.batt_v_in_gpio);
	::setInput(pins.i2c_scl);
	::setInput(pins.i2c_sda);

	printWakeupReason();

	auto len =
	    settings::get("board", "display", displayModel, sizeof(displayModel));
	if (len == 0) {
		ESP_LOGE(TAG, "Could not get display type");
		// Default screen for old boards
		strcpy(displayModel, "ED060XC3");
	} else {
		ESP_LOGI(TAG, "Display is a %s", displayModel);
	}
}

board::ChargeStatus board::chargeStatus() {
	if (boardRevision == 1) {
		// Board does not have charging status, always return charging if externally
		// powered
		if (powerSource() == PowerSource::External) {
			return ChargeStatus::Charging;
		}
		return ChargeStatus::NotCharging;
	} else {
		auto level = epd_gpio_get_level_v6();
		auto chrg = (level & pins.bat_chrg) == 0;
		auto stdby = (level & pins.bat_stdby) == 0;
		if (chrg) {
			return ChargeStatus::Charging;
		}
		if (!chrg && stdby) {
			return ChargeStatus::ChargeComplete;
		}
		return ChargeStatus::NotCharging;
	}
}

void board::powerDown(uint64_t seconds) {
	if (::screen) {
		::screen->powerOff();
		::screen->deinit();
	}

	// Turn off everything to minimize current consumption during sleep
	esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_AUTO);
	esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_SLOW_MEM, ESP_PD_OPTION_AUTO);
	esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM, ESP_PD_OPTION_AUTO);
	esp_sleep_pd_config(ESP_PD_DOMAIN_XTAL, ESP_PD_OPTION_AUTO);
	esp_sleep_pd_config(ESP_PD_DOMAIN_RTC8M, ESP_PD_OPTION_AUTO);
	esp_sleep_pd_config(ESP_PD_DOMAIN_VDDSDIO, ESP_PD_OPTION_AUTO);

	esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_ALL);

	if (seconds > 0) {
		// Configure how long to sleep
		esp_sleep_enable_timer_wakeup(seconds * uS_TO_S_FACTOR);
	}
	if (board::powerSource() == board::PowerSource::Battery) {
		// Wakeup on external power connected
		esp_sleep_enable_ext0_wakeup(pins.ext_pwr_detect, 1);
	} else {
		// Turn off wakeup on periph since we are running on external power
		esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
	}
	ESP_LOGI(TAG, "Setup ESP32 to sleep for every %llu seconds", seconds);

	esp_deep_sleep_start();
	// This function never returns
}

board::PowerSource board::powerSource() {
	return gpio_get_level(pins.ext_pwr_detect) ? board::PowerSource::External
	                                           : board::PowerSource::Battery;
}

float board::batteryVoltage() {
	esp_adc_cal_characteristics_t adcChars;
	esp_adc_cal_value_t valType = esp_adc_cal_characterize(
	    ADC_UNIT_1, ADC_ATTEN_DB_0, ADC_WIDTH_BIT_12, 1100, &adcChars);
	if (valType == ESP_ADC_CAL_VAL_EFUSE_TP) {
		printf("Characterized using Two Point Value\n");
	} else if (valType == ESP_ADC_CAL_VAL_EFUSE_VREF) {
		printf("Characterized using eFuse Vref\n");
	} else {
		printf("Characterized using Default Vref\n");
	}

	adc1_config_width(ADC_WIDTH_12Bit);
	adc1_config_channel_atten(
	    pins.batt_v_in_adc, ADC_ATTEN_DB_0);  // set reference voltage to internal

	uint32_t adcValue = 0;
	// Multisampling
	constexpr auto NO_OF_SAMPLES = 64;
	for (int i = 0; i < NO_OF_SAMPLES; i++) {
		adcValue += adc1_get_raw(pins.batt_v_in_adc);
	}
	adcValue /= NO_OF_SAMPLES;

	// adcRange is 100-950mV
	uint32_t cvoltage = esp_adc_cal_raw_to_voltage(adcValue, &adcChars);
	return cvoltage / 1000.0 * batteryAdcFactor();
}

uint8_t board::revision() {
	return ::boardRevision;
}

std::string_view board::screenModel() {
	return ::displayModel;
}
