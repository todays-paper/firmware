#include "ota.h"

#include <cJSON.h>
#include <esp_crt_bundle.h>
#include <esp_http_client.h>
#include <esp_https_ota.h>
#include <esp_log.h>
#include <esp_ota_ops.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <mbedtls/md.h>
#include <string.h>

#include "https.h"

namespace {
	const char *TAG = "OTA";
	constexpr auto HASH_LEN = 32;
	char spiffsHash[HASH_LEN * 2 + 1];

	constexpr auto BUFFSIZE = SPI_FLASH_SEC_SIZE;
	char EXT_RAM_ATTR buffer[BUFFSIZE];

	cJSON *json = nullptr;

	const esp_partition_t *findSpiffsPartition() {
		const esp_partition_t *spiffs_partition = NULL;
		auto it = esp_partition_find(ESP_PARTITION_TYPE_DATA,
		                             ESP_PARTITION_SUBTYPE_DATA_SPIFFS, NULL);
		if (it != NULL) {
			spiffs_partition = esp_partition_get(it);
		}

		esp_partition_iterator_release(it);
		return spiffs_partition;
	}

	const char *getUrlForImage(const char *image) {
		if (!json) {
			return nullptr;
		}
		auto app = cJSON_GetObjectItem(json, image);
		if (!app) {
			return nullptr;
		}
		auto url = cJSON_GetObjectItem(app, "url");
		if (!cJSON_IsString(url) || (url->valuestring == NULL)) {
			return nullptr;
		}
		return url->valuestring;
	}

	bool updateFirmware() {
		const char *url = getUrlForImage("app");
		if (!url) {
			ESP_LOGW(TAG, "No url found to download");
			return false;
		}
		ESP_LOGI(TAG, "Download %s", url);
		esp_http_client_config_t config = {
		    .url = url,
		    .crt_bundle_attach = esp_crt_bundle_attach,
		};

		ESP_LOGI(TAG, "Attempting to download update from %s", config.url);
		esp_err_t ret = esp_https_ota(&config);
		if (ret == ESP_OK) {
			ESP_LOGE(TAG, "Firmware upgrade success");
			return true;
		}
		return false;
	}  // namespace

	static void printSha256(const uint8_t *image_hash, const char *label) {
		char hash_print[HASH_LEN * 2 + 1];
		hash_print[HASH_LEN * 2] = 0;
		for (int i = 0; i < HASH_LEN; ++i) {
			sprintf(&hash_print[i * 2], "%02x", image_hash[i]);
		}
		ESP_LOGI(TAG, "%s %s", label, hash_print);
	}

	bool updateForPartition(cJSON *json, const char *runningVersion) {
		if (!json) {
			return false;
		}
		auto version = cJSON_GetObjectItem(json, "version");
		if (!cJSON_IsString(version) || (version->valuestring == NULL)) {
			ESP_LOGW(TAG, "no version");

			return false;
		}
		ESP_LOGI(TAG, "version to download: %s, running %s", version->valuestring,
		         runningVersion);
		if (strcmp(version->valuestring, runningVersion) == 0) {
			return false;
		}
		ESP_LOGI(TAG, "update available");
		return true;
	}

	bool updateSPIFFS() {
		const char *url = getUrlForImage("data");
		if (!url) {
			ESP_LOGW(TAG, "No url found to download");
			return false;
		}
		const esp_partition_t *spiffs_partition = findSpiffsPartition();
		esp_http_client_config_t config = {
		    .url = url,
		    .crt_bundle_attach = esp_crt_bundle_attach,
		};
		esp_http_client_handle_t client = esp_http_client_init(&config);
		if (client == NULL) {
			ESP_LOGE(TAG, "Failed to initialise HTTP connection");
			return false;
		}
		auto err = esp_http_client_open(client, 0);
		if (err != ESP_OK) {
			ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
			esp_http_client_cleanup(client);
			return false;
		}
		esp_http_client_fetch_headers(client);

		// Delete SPIFFS Partition
		err =
		    esp_partition_erase_range(spiffs_partition, 0, spiffs_partition->size);
		ESP_ERROR_CHECK(err);

		int binary_file_length = 0;
		while (1) {
			int data_read = esp_http_client_read(client, buffer, BUFFSIZE);
			if (data_read < 0) {
				ESP_LOGE(TAG, "Error: SSL data read error");
				esp_http_client_cleanup(client);
				return false;
			} else if (data_read > 0) {
				// WRITE SPIFFS PARTITION
				err = esp_partition_write(spiffs_partition, binary_file_length,
				                          (const void *)buffer, data_read);

				if (err != ESP_OK) {
					esp_http_client_cleanup(client);
					return false;
				}

				binary_file_length += data_read;
				ESP_LOGI(TAG, "Written image length %d", binary_file_length);
			} else if (data_read == 0) {
				ESP_LOGI(TAG, "Connection closed,all data received");
				break;
			}
		}
		ESP_LOGI(TAG, "Total Write binary data length : %d", binary_file_length);
		return true;
	}

	void requestVersions() {
		auto success =
		    https::get("https://fw.todays-paper.ink/epdiy_v5/firmware.json", buffer,
		               sizeof(buffer));
		if (!success) {
			ESP_LOGW(TAG, "Could not download firmware update");
			return;
		}
		json = cJSON_Parse(buffer);

		if (json == NULL) {
			ESP_LOGW(TAG, "Could not decode json data");
		}
	}
}  // namespace

void ota::init() {
	uint8_t sha256[HASH_LEN] = {0};

	esp_partition_get_sha256(findSpiffsPartition(), sha256);
	printSha256(sha256, "SPIFFS: ");
	spiffsHash[HASH_LEN * 2] = '\0';
	for (int i = 0; i < HASH_LEN; ++i) {
		sprintf(&spiffsHash[i * 2], "%02x", sha256[i]);
	}
}

void ota::deinit() {
	if (::json) {
		cJSON_Delete(::json);
		::json = nullptr;
	}
}

void ota::fetchVersions() {
	::requestVersions();
}

bool ota::firmwareUpdateAvailable() {
	if (!::json) {
		return false;
	}
	auto *desc = esp_ota_get_app_description();
	auto app = cJSON_GetObjectItem(::json, "app");
	return ::updateForPartition(app, desc->version);
}

bool ota::spiffsUpdateAvailable() {
	if (!::json) {
		return false;
	}
	auto data = cJSON_GetObjectItem(json, "data");
	return ::updateForPartition(data, spiffsHash);
}

bool ota::updateFirmware() {
	return ::updateFirmware();
}

bool ota::updateSpiffs() {
	return ::updateSPIFFS();
}
