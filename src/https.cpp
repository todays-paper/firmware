#include "https.h"

#include <esp_err.h>
#include <stdint.h>
#include <string.h>

#include "esp_crt_bundle.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "esp_tls.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

namespace {
#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 2048
	static const char *TAG = "Http";

	struct DownloadData {
		char *buffer;
		size_t len;
		size_t received;
		const https::HeaderCB headerCB;
	};

	esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
		switch (evt->event_id) {
			case HTTP_EVENT_ERROR:
				ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
				break;
			case HTTP_EVENT_ON_CONNECTED:
				ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
				break;
			case HTTP_EVENT_HEADER_SENT:
				ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
				break;
			case HTTP_EVENT_ON_HEADER:
				ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key,
				         evt->header_value);
				if (evt->user_data) {
					DownloadData *data = reinterpret_cast<DownloadData *>(evt->user_data);
					if (data->headerCB) {
						data->headerCB(evt->header_key, evt->header_value);
					}
				}
				break;
			case HTTP_EVENT_ON_DATA:
				ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
				/*
				 *  Check for chunked encoding is added as the URL for chunked encoding
				 * used in this example returns binary data. However, event handler can
				 * also be used in case chunked encoding is used.
				 */
				if (!esp_http_client_is_chunked_response(evt->client)) {
					// If user_data buffer is configured, copy the response into the
					// buffer
					if (!evt->user_data) {
						break;
					}
					DownloadData *data = (DownloadData *)evt->user_data;
					data->received += evt->data_len;
					if (data->received > data->len) {
						// Buffer overflow, got more data than we could handle
						return ESP_FAIL;
					}
					memcpy(data->buffer + data->received - evt->data_len, evt->data,
					       evt->data_len);
				}

				break;
			case HTTP_EVENT_ON_FINISH:
				ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
				break;
			case HTTP_EVENT_DISCONNECTED:
				ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
				int mbedtls_err = 0;
				esp_err_t err = esp_tls_get_and_clear_last_error(
				    (esp_tls_error_handle_t)evt->data, &mbedtls_err, NULL);
				if (err != 0) {
					ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
					ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
				}
				break;
		}
		return ESP_OK;
	}
}  // namespace

bool https::get(const char *url, char *buffer, size_t buffer_len) {
	DownloadData data{
	    .buffer = buffer,
	    .len = buffer_len,
	    .received = 0,
	    .headerCB = nullptr,
	};
	esp_http_client_config_t config = {
	    .url = url,
	    .event_handler = _http_event_handler,
	    .user_data = &data,
	    .crt_bundle_attach = esp_crt_bundle_attach,
	};
	ESP_LOGI(TAG, "Connecting to %s", config.url);
	esp_http_client_handle_t client = esp_http_client_init(&config);
	esp_err_t err = esp_http_client_perform(client);

	bool retval = false;
	if (err == ESP_OK) {
		ESP_LOGI(TAG, "HTTPS Status = %d, content_length = %d",
		         esp_http_client_get_status_code(client),
		         esp_http_client_get_content_length(client));
		if (esp_http_client_get_status_code(client) == 200 &&
		    data.received <= data.len) {
			retval = true;
		} else {
			ESP_LOGE(TAG, "%d != 200 or %d > %d",
			         esp_http_client_get_status_code(client), data.received,
			         data.len);
		}
	} else {
		ESP_LOGE(TAG, "Error perform http request %s", esp_err_to_name(err));
	}
	esp_http_client_cleanup(client);
	return retval;
}

bool https::post(const char *url, const cJSON *postData, char *buffer,
                 size_t buffer_len, const HeaderCB &headerCB) {
	DownloadData data{
	    .buffer = buffer,
	    .len = buffer_len,
	    .received = 0,
	    .headerCB = headerCB,
	};
	esp_http_client_config_t config = {
	    .url = url,
	    .method = HTTP_METHOD_POST,
	    .event_handler = _http_event_handler,
	    .user_data = &data,
	    .crt_bundle_attach = esp_crt_bundle_attach,
	};
	char *postDataString = cJSON_Print(postData);
	ESP_LOGI(TAG, "Connecting to %s", config.url);
	esp_http_client_handle_t client = esp_http_client_init(&config);
	esp_http_client_set_header(client, "Content-Type", "application/json");
	esp_http_client_set_post_field(client, postDataString,
	                               strlen(postDataString));
	esp_err_t err = esp_http_client_perform(client);
	free(postDataString);

	bool retval = false;
	if (err == ESP_OK) {
		ESP_LOGI(TAG, "HTTPS Status = %d, content_length = %d",
		         esp_http_client_get_status_code(client),
		         esp_http_client_get_content_length(client));
		if (esp_http_client_get_status_code(client) == 200 &&
		    data.received <= data.len) {
			retval = true;
		}
	} else {
		ESP_LOGE(TAG, "Error perform http request %s", esp_err_to_name(err));
	}
	esp_http_client_cleanup(client);
	return retval;
}
