#include "app.h"
#include "esp_event.h"
#include "settings.h"


extern "C" void app_main(void) {
	settings::init();
	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());

	appStart();
}
