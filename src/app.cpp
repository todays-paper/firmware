#include "app.h"

#include <cJSON.h>

#include "board.h"
#include "config.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_sleep.h"
#include "esp_spiffs.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "https.h"
#include "img.h"
#include "ota.h"
#include "screen.h"
#include "settings.h"
#include "uuid.h"
#include "webserver.h"
#include "wifi.h"

using namespace uuid;

namespace {
	constexpr auto STACK_SIZE = configMINIMAL_STACK_SIZE * 10;
	const char *TAG = "App";
	uuid_t uuid;
	uint64_t sleepTime = 60 * 60;
	// Set this to the max size this board supports
	constexpr auto SCREEN_WIDTH = 1448;
	constexpr auto SCREEN_HEIGHT = 1072;
	char EXT_RAM_ATTR
	    buffer[SCREEN_WIDTH * SCREEN_HEIGHT / 2 + sizeof(ImgHeader)];

	StaticTask_t taskBuffer;
	StackType_t stack[STACK_SIZE];
	TaskHandle_t taskHandle;
	EpdiyScreen screen;

	void checkOTA();
	void enableVcom();
	void fetchAndDisplay();
	void initSpiffs();
	void showStaticImage(const char *path);

	void appBattery() {
		screen.init(EPD_OPTIONS_DEFAULT, board::screenModel());
		if (!wifi::isProvisioned()) {
			initSpiffs();
			showStaticImage("/spiffs/factory_battery.epdiy");
			board::powerDown(0);  // Sleep until power inserted
		}

		// On battery, normal mode
		wifi::start();
		fetchAndDisplay();
		board::powerDown(sleepTime);
	}

	void appPowered() {
		// When running on power we need more ram to allow provisioning, ota etc.
		// So we sacrifice performance of EPDIY to have more room.
		screen.init(EpdInitOptions::EPD_LUT_1K, board::screenModel());
		// Externally powered
		if (!wifi::isProvisioned()) {
			wifi::startProvisioning();
			screen.renderFromFramebuffer();
			while (!wifi::isConnected()) {
				if (board::powerSource() == board::PowerSource::Battery) {
					// Abort provisioning and return to sleep
					board::powerDown(1);
				}
				vTaskDelay(pdMS_TO_TICKS(100));
			}
		} else {
			wifi::start();
		}
		if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT0) {
			// Wakeup by connecting external power. Check for firmware update.
			checkOTA();
		}
		initSpiffs();

		showStaticImage("/spiffs/external_power.epdiy");

		// Start webserver to accept configuration commands
		webserver::start();

		while (1) {
			int tmr = 0;
			while (tmr++ < 20) {
				vTaskDelay(pdMS_TO_TICKS(1000));
				if (board::powerSource() == board::PowerSource::Battery) {
					// Switched to battery, sleep shortly
					board::powerDown(1);
				}
			}
		}
	}

	void appTask(__attribute__((unused)) void *param) {
		wifi::init();

		size_t len =
		    settings::get("board", "uuid", (uint8_t *)&uuid.val, sizeof(uuid.val));
		if (len == 0) {
			// No uuid stored in flash. Generate one.
			uuid = uuid_t::new_v4();
			settings::write("board", "uuid", (uint8_t *)uuid.val, sizeof(uuid.val));
		}

		if (board::powerSource() == board::PowerSource::Battery) {
			appBattery();
		} else {
			appPowered();
		}

		board::powerDown(0);
	}

	void checkOTA() {
		ota::init();
		ota::fetchVersions();
		bool fwUpdate = ota::firmwareUpdateAvailable();
		bool dataUpdate = ota::spiffsUpdateAvailable();
		if (fwUpdate == false && dataUpdate == false) {
			ota::deinit();
			return;
		}
		initSpiffs();
		showStaticImage("/spiffs/firmware_update.epdiy");
		bool restartNeeded = false;
		if (fwUpdate) {
			restartNeeded = ota::updateFirmware();
		}
		if (dataUpdate) {
			// Maybe we need to close spiffs again here?
			ota::updateSpiffs();
		}
		if (restartNeeded) {
			esp_restart();
		}
		ota::deinit();
	}

	[[maybe_unused]] void enableVcom() {
		screen.init(EPD_OPTIONS_DEFAULT, board::screenModel());
		ESP_LOGI(TAG, "waiting for one second before poweron...");
		vTaskDelay(pdMS_TO_TICKS(1000));
		ESP_LOGI(TAG, "enabling VCOMM...");
		screen.powerOn();
		ESP_LOGI(
		    TAG,
		    "VCOMM enabled. You can now adjust the on-board trimmer to the VCOM "
		    "value specified on the display.");
		while (1) {
			vTaskDelay(pdMS_TO_TICKS(1000));
		};
	}

	void fetchAndDisplay() {
		char url[256];
		auto len = settings::get("nvs", "fetch_url", url, sizeof(url));
		if (len == 0) {
			ESP_LOGE(TAG, "Could not get fetch url");
			return;
		}
		auto *desc = esp_ota_get_app_description();
		uuid_str_t uuidString;
		uuid.to_str(uuidString);
		cJSON *data = cJSON_CreateObject();
		cJSON_AddStringToObject(data, "format", "epdiy");
		cJSON_AddStringToObject(data, "uuid", uuidString);
		cJSON_AddNumberToObject(data, "battery", board::batteryVoltage());
		cJSON_AddStringToObject(data, "version", desc->version);
		for (auto i = 0; i < 5; ++i) {
			auto success = https::post(url, data, buffer, sizeof(buffer),
			                           [](const char *key, const char *value) {
				                           if (strcasecmp(key, "cache-control") != 0) {
					                           return;
				                           }
				                           uint64_t maxAge;
				                           auto ret =
				                               sscanf(value, "max-age=%llu", &maxAge);
				                           if (ret == 1) {
					                           sleepTime = maxAge;
				                           }
			                           });
			if (success) {
				ESP_LOGI(TAG, "Download success");
				imgRenderBuffer(buffer);
				screen.renderFromFramebuffer();
				break;
			}
			ESP_LOGE(TAG, "Download failed");
			vTaskDelay(pdMS_TO_TICKS(1000));
		}
		cJSON_free(data);
	}

	void initSpiffs() {
		esp_vfs_spiffs_conf_t conf = {
		    .base_path = "/spiffs",
		    .partition_label = NULL,
		    .max_files = 5,
		    .format_if_mount_failed = false,
		};
		esp_err_t ret = esp_vfs_spiffs_register(&conf);
		if (ret != ESP_OK) {
			if (ret == ESP_FAIL) {
				ESP_LOGE(TAG, "Failed to mount or format filesystem");
			} else if (ret == ESP_ERR_NOT_FOUND) {
				ESP_LOGE(TAG, "Failed to find SPIFFS partition");
			} else {
				ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
			}
		}
	}

	bool loadImage(const char *path) {
		FILE *fd = fopen(path, "r");
		if (fd == NULL) {
			ESP_LOGE(TAG, "Failed to open file %s for reading", path);
			return false;
		}
		auto size = fread(buffer, 1, sizeof(buffer), fd);

		fclose(fd);
		return true;
	}

	void showStaticImage(const char *path) {
		if (!loadImage(path)) {
			return;
		}
		imgRenderBuffer(buffer);
		screen.renderFromFramebuffer();
	}
}  // namespace

void appStart() {
	board::init(&screen);

	taskHandle = xTaskCreateStaticPinnedToCore(::appTask, "App", STACK_SIZE, NULL,
	                                           configTASK_PRIO_NORMAL, stack,
	                                           &taskBuffer, tskNO_AFFINITY);
	assert(taskHandle);
}
