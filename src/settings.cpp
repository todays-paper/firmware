#include <esp_log.h>
#include <nvs_flash.h>
#include <settings.h>

namespace {
	const char *TAG = "NVS";
}

settings::Error settings::get(const char *partition, const char *key,
                              uint16_t *dest) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READONLY,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return settings::Error::ERROR;
	}

	err = nvs_get_u16(nvs_handle, key, dest);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) reading key %s!\n", esp_err_to_name(err), key);
		return settings::Error::ERROR;
	}
	return settings::Error::OK;
}

settings::Error settings::get(const char *partition, const char *key,
                              uint8_t *dest) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READONLY,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return settings::Error::ERROR;
	}

	err = nvs_get_u8(nvs_handle, key, dest);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) reading key %s!\n", esp_err_to_name(err), key);
		return settings::Error::ERROR;
	}
	return settings::Error::OK;
}

size_t settings::get(const char *partition, const char *key, char *dest,
                     size_t dest_len) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READONLY,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return 0;
	}

	size_t size = dest_len;
	err = nvs_get_str(nvs_handle, key, dest, &size);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) reading key %s!\n", esp_err_to_name(err), key);
		if (err == ESP_ERR_NVS_INVALID_LENGTH) {
			nvs_get_str(nvs_handle, key, NULL, &size);
			ESP_LOGE(TAG, "The length must be at least %zu bytes", size);
		}
		return 0;
	}
	return size;
}

size_t settings::get(const char *partition, const char *key, uint8_t *dest,
                     size_t dest_len) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READONLY,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return 0;
	}

	size_t size = dest_len;
	err = nvs_get_blob(nvs_handle, key, dest, &size);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) reading key %s!\n", esp_err_to_name(err), key);
		if (err == ESP_ERR_NVS_INVALID_LENGTH) {
			nvs_get_str(nvs_handle, key, NULL, &size);
			ESP_LOGE(TAG, "The length must be at least %zu bytes", size);
		}
		return 0;
	}
	return size;
}

bool settings::write(const char *partition, const char *key, uint16_t value) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READWRITE,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return false;
	}

	err = nvs_set_u16(nvs_handle, key, value);

	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) writing key %s!\n", esp_err_to_name(err), key);
		return false;
	}
	return true;
}

bool settings::write(const char *partition, const char *key,
                     const char *value) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READWRITE,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return false;
	}

	err = nvs_set_str(nvs_handle, key, value);

	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) writing key %s!\n", esp_err_to_name(err), key);
		return false;
	}
	return true;
}

bool settings::write(const char *partition, const char *key,
                     const uint8_t *value, size_t len) {
	nvs_handle_t nvs_handle;
	auto err = nvs_open_from_partition(partition, "todays_paper", NVS_READWRITE,
	                                   &nvs_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) opening nvs partition: %s!\n",
		         esp_err_to_name(err), partition);
		return false;
	}

	err = nvs_set_blob(nvs_handle, key, value, len);

	if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error (%s) writing key %s!\n", esp_err_to_name(err), key);
		return false;
	}
	return true;
}

void settings::init() {
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
	    ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	ESP_ERROR_CHECK(nvs_flash_init_partition("board"));
}
