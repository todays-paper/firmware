#pragma once

namespace wifi {
	void init();
	bool isConnected();
	bool isProvisioned();
	void reset();
	void start();
	void startProvisioning();
}  // namespace wifi
