#pragma once

#include <screen.h>

#include <cinttypes>

namespace board {
	enum class PowerSource {
		External,
		Battery,
	};
	enum class ChargeStatus {
		NotCharging,  // Probably running on battery
		Charging,
		ChargeComplete,  // Battery full
	};

	void init(IScreen *screen);
	ChargeStatus chargeStatus();
	float batteryVoltage();
	void powerDown(uint64_t seconds);
	PowerSource powerSource();
	uint8_t revision();
	std::string_view screenModel();
}  // namespace board
