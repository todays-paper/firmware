#pragma once

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 *
 * See http://www.freertos.org/a00110.html.
 *----------------------------------------------------------*/

#define configTASK_PRIO_REALTIME (configMAX_PRIORITIES - 1)
#define configTASK_PRIO_ABOVE_NORMAL (configMAX_PRIORITIES - 2)
#define configTASK_PRIO_NORMAL (configMAX_PRIORITIES - 3)
#define configTASK_PRIO_BELOW_NORMAL (configMAX_PRIORITIES - 4)
#define configTASK_PRIO_LOW (configMAX_PRIORITIES - 5)
