#pragma once
#include <stdbool.h>

namespace ota {
	void init();
	void deinit();
	void fetchVersions();

	bool firmwareUpdateAvailable();
	bool spiffsUpdateAvailable();

	bool updateFirmware();
	bool updateSpiffs();
}  // namespace ota
