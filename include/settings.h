#pragma once

#include <stddef.h>

namespace settings {
	enum class Error {
		OK = 0,
		ERROR = 1,
	};
	Error get(const char *partition, const char *key, uint16_t *dest);
	Error get(const char *partition, const char *key, uint8_t *dest);
	size_t get(const char *partition, const char *key, char *dest,
	           size_t dest_len);
	size_t get(const char *partition, const char *key, uint8_t *dest,
	           size_t dest_len);
	void init();
	bool write(const char *partition, const char *key, uint16_t value);
	bool write(const char *partition, const char *key, const char *value);
	bool write(const char *partition, const char *key, const uint8_t *value,
	           size_t len);
}  // namespace settings
