#pragma once

#include <cJSON.h>
#include <stddef.h>

#include <functional>

namespace https {
	using HeaderCB = std::function<void(const char *, const char *)>;
	bool get(const char *url, char *buffer, size_t buffer_len);
	bool post(const char *url, const cJSON *data, char *buffer, size_t buffer_len,
	          const HeaderCB &headerCB);
}  // namespace https
