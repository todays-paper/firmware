#pragma once

#include <qrcode.h>

#include <cinttypes>

#include "epd_highlevel.h"

struct ImgHeader {
	uint32_t size;
	uint16_t width;
	uint16_t height;
	uint8_t bpp;
	uint32_t dataSize;
};

void imgInit(EpdiyHighlevelState *hl);
void imgRenderBuffer(const char *buffer);
void renderProvisioning(esp_qrcode_handle_t qrcode);
