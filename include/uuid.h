#pragma once

#include <cinttypes>
#include <cstring>

namespace uuid {
	using uuid_str_t = char[37];
	struct uuid_t {
		int to_str(char *buffer, size_t length);
		int to_str(uuid_str_t buffer);
		bool parse_str(const char *str);
		static uuid_t new_v4();

		uint8_t val[16];
	};
}  // namespace uuid
