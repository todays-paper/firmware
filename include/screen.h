#pragma once

#include <epd_highlevel.h>

#include <string>

class IScreen {
 public:
	virtual void deinit() = 0;
	virtual void powerOn() = 0;
	virtual void powerOff() = 0;
	virtual void renderFromFramebuffer() = 0;
};

class EpdiyScreen : public IScreen {
 public:
	EpdiyScreen();
	virtual ~EpdiyScreen() = default;

	virtual void deinit() override;
	void init(EpdInitOptions options, std::string_view model);
	virtual void powerOn() override;
	virtual void powerOff() override;
	virtual void renderFromFramebuffer() override;

 private:
	enum class State {
		DEACTIVATED,
		POWERED_OFF,
		POWERED_ON,
	};
	EpdiyHighlevelState m_hl;
	State m_state = State::DEACTIVATED;
};
